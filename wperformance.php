<?php
/*
Plugin Name: WPerformance
Plugin URI: https://spaggel.nl
Description: Improve WordPress asset performance
Author: Daniël van der Linden
Version: 0.2.1
Author URI: https://spaggel.nl
Textdomain: wperformance
*/

$options = get_option( 'dwpf_settings' );

define('WPERFORMANCE_CACHE_FOLDER', WP_CONTENT_DIR . '/cache/wperformance');
define('WPERFORMANCE_CACHE_URL', WP_CONTENT_URL . '/cache/wperformance');
define('WPPERFORMANCE_CLEAR_CACHE', isset($_GET['wperformance']) && $_GET['wperformance'] == 'invalidate');

$dwpf_combined_filename = 'styles.css';
if(isset($options['dwpf_combined_filename']) && !empty($options['dwpf_combined_filename'])) {
	$dwpf_combined_filename = sanitize_text_field($options['dwpf_combined_filename']);
}
define('WPERFORMANCE_STYLES_NAME', $dwpf_combined_filename);

// path to stylesheet
define('WPERFORMANCE_STYLES_PATH', WPERFORMANCE_CACHE_FOLDER . DIRECTORY_SEPARATOR . WPERFORMANCE_STYLES_NAME);

// url to stylesheet
define('WPERFORMANCE_STYLES_URL', WPERFORMANCE_CACHE_URL. DIRECTORY_SEPARATOR . WPERFORMANCE_STYLES_NAME);

// timeout in seconds
$dwpf_cache_lifetime = 86400;
if(isset($options['dwpf_cache_lifetime']) && !empty($options['dwpf_cache_lifetime'])) {
	$dwpf_cache_lifetime = (int)$options['dwpf_cache_lifetime'];
}
define('WPERFORMANCE_CACHE_TIMEOUT', $dwpf_cache_lifetime); // a day

// set to true to completely disable this plugin
$dwpf_is_enabled = isset($options['dwpf_is_enabled']) && !empty($options['dwpf_is_enabled']) ? $options['dwpf_is_enabled'] : false;
define('WPERFORMANCE_DISABLED', !$dwpf_is_enabled);

// set to true if you dont want to use WordPress emojis
$dwpf_disable_emoji = isset($options['dwpf_disable_emoji']) && !empty($options['dwpf_disable_emoji']) ? $options['dwpf_disable_emoji'] : false;
define('WP_DISABLE_EMOJIS', $dwpf_disable_emoji);

$dwpf_css_load_type = isset($options['dwpf_stylesheet_load_type']) ? $options['dwpf_stylesheet_load_type'] : '';
define('WPERFORMANCE_CSS_LOAD_TYPE', $dwpf_css_load_type);
/**
 * @var $optimizer \Donool\WPerformance\Optimizer
 */
$optimizer = null;

if(WPPERFORMANCE_CLEAR_CACHE) {
	@unlink(WPERFORMANCE_STYLES_PATH);
	header('Location: ?page=wperformance&wperformance=cache_cleared');
	die;
}

// prevent loading cached files when plugin is disabled or when the user is in the admin section (wp-admin)
if(!WPERFORMANCE_DISABLED && !is_admin()) {
    add_action('init', 'start');

	// If CSS load type is deferred we need an extra script to allow deferred loading
	if(WPERFORMANCE_CSS_LOAD_TYPE == 'deferred') {
		add_action('wp_head','add_load_css', 7);
	}
}
function start() {
	global $optimizer;
	// load the needed files
	require_once 'vendor/autoload.php';
	$optimizer = new \Donool\WPerformance\Optimizer();

    // hook in to fontsprint scripts to print or own combined css
    add_action('wp_print_scripts', 'combine', 999);

    // hook in to the style_loader_tag filter because this generates the link tag and puts in in the HTML
    add_action('style_loader_tag', 'prevent_output_and_combine_styles', 10);
}

function add_load_css(){
	?><script>function loadCSS( href, before, media ){ "use strict"; var ss = window.document.createElement( "link" ); var ref = before || window.document.getElementsByTagName( "script" )[ 0 ]; ss.rel = "stylesheet"; ss.href = href; ss.media = "only x"; ref.parentNode.insertBefore( ss, ref ); setTimeout( function(){ ss.media = media || "all"; } ); return ss; }</script><?php
}

function combine()
{
    global $optimizer;

    if(!file_exists(WPERFORMANCE_CACHE_FOLDER)) {
        mkdir(WPERFORMANCE_CACHE_FOLDER, 0777, true);
    }

    // check if files are redownloaded to prevent putting empty css in the cached file
    if($optimizer->needsRedownload()) {
        $output = '/* ' . PHP_EOL .
                  ' * CREATED BY WPERFORMANCE AT ' . date('Y-m-d H:i:s') . PHP_EOL .
                  ' * Visit https://github.com/Donool/WPerformance for more info' . PHP_EOL .
                  ' */' . PHP_EOL .
                  $optimizer->getCss();
        file_put_contents(WPERFORMANCE_STYLES_PATH, $output);
    }

    // render the link to the combined stylesheet
	switch (WPERFORMANCE_CSS_LOAD_TYPE) {
		case 'deferred':
			// TODO: ADD DEFERRED CSS LOADING
			echo "<script>loadCSS('" . WPERFORMANCE_STYLES_URL . "');</script>\n";
			break;
		case '':
		case 'default':
		default:
			echo sprintf('<link href="%s" rel="stylesheet">', WPERFORMANCE_STYLES_URL);
			break;
	}
}


/**
 * @usage Provide a HTML <link href="#" /> tag
 * @param string $link HTML link tag
 * @return string
 */
function prevent_output_and_combine_styles($link) {
	global $optimizer;

    if(!$optimizer->needsRedownload()) {
        return '';
    }

    $xml = simplexml_load_string($link);
    $href = (string)$xml->attributes()->href;
    $href = str_replace('.serv01', '.nl', $href); // TODO: REMOVE THIS LATER, this exists because of a problem in PHP in combination with a fake tld on a VM

    $parsedUrl = parse_url($href);
    if(!isset($parsedUrl['scheme'])) {
        // if we dont have a scheme append http scheme
        $href = is_ssl() ? 'https://' . $href : 'http://' . $href;
        // make sure we never have 4 slashes (e.g. for urls like //fonts.googleapis.com this would be http:////fonts.googleapis.com
        $href = str_replace('////', '//' ,$href);
    }

    $source = download($href);

	if($source !== false) {

    	if(shouldFixPaths($href)) {
		    $source = $optimizer->fixTemplatePaths($source, $href);
	    }

	    $optimizer->addStylesheet($source);
    }

    return '';
}

/**
 * Check if the stylesheet should be processed by the relative url fixer
 * @param $source
 *
 * @return bool
 */
function shouldFixPaths($source) {
	return strpos($source, get_site_url()) !== false;
}

/**
 * Download the file
 * @param        $link
 * @param string $type
 *
 * @return bool|string
 */
function download($link, $type = 'css') {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_URL, $link);
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}

/**
 * Check if we want to remove WP emojis
 */
if(WP_DISABLE_EMOJIS && !WPERFORMANCE_DISABLED) {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
}

add_action( 'admin_menu', 'dwpf_add_admin_menu' );
add_action( 'admin_init', 'dwpf_settings_init' );


/**
 * Setup options page and settings
 */
function dwpf_add_admin_menu(  ) {

	add_options_page( 'WPerformance', 'WPerformance', 'manage_options', 'wperformance', 'dwpf_options_page' );

}


function dwpf_settings_init(  ) {

	register_setting( 'pluginPage', 'dwpf_settings' );

	add_settings_section(
		'dwpf_pluginPage_section',
		__( 'Instellingen', 'wperformance' ),
		'dwpf_settings_section_callback',
		'pluginPage'
	);

	add_settings_field(
		'dwpf_is_enabled',
		__( 'Inschakelen', 'wperformance' ),
		'dwpf_is_enabled_render',
		'pluginPage',
		'dwpf_pluginPage_section'
	);

	add_settings_field(
		'dwpf_stylesheet_load_type',
		__( 'CSS laadmethode', 'wperformance' ),
		'dwpf_stylesheet_load_type_render',
		'pluginPage',
		'dwpf_pluginPage_section'
	);

	add_settings_field(
		'dwpf_combined_filename',
		__( 'Gecombineerde stylesheet naam', 'wperformance' ),
		'dwpf_combined_filename_render',
		'pluginPage',
		'dwpf_pluginPage_section'
	);

	add_settings_field(
		'dwpf_cache_lifetime',
		__( 'Cache lifetime (in seconden)', 'wperformance' ),
		'dwpf_cache_lifetime_render',
		'pluginPage',
		'dwpf_pluginPage_section'
	);

	add_settings_field(
		'dwpf_disable_emoji',
		__( 'WP Emoji uitschakelen', 'wperformance' ),
		'dwpf_disable_emoji_render',
		'pluginPage',
		'dwpf_pluginPage_section'
	);

	add_settings_field(
		'dwpf_invalidate_cache',
		__( 'Cache legen', 'wperformance' ),
		'dwpf_invalidate_cache_render',
		'pluginPage',
		'dwpf_pluginPage_section'
	);

}


function dwpf_is_enabled_render(  ) {

	$options = get_option( 'dwpf_settings' );
	$checked = false;
	if(isset($options['dwpf_is_enabled']) && $options['dwpf_is_enabled'] == '1') {
		$checked = true;
	}
	?>
	<input type='checkbox' name='dwpf_settings[dwpf_is_enabled]' <?php checked( $checked, 1 ); ?> value='1'>
	<?php

}

function dwpf_stylesheet_load_type_render(  ) {

	$options = get_option( 'dwpf_settings' );
	$selected = isset($options['dwpf_stylesheet_load_type']) ? $options['dwpf_stylesheet_load_type'] : '';
	$htmlOptions = array(
		'default' => 'Standaard',
		'deferred' => 'Uitgesteld'
	);
	?>
	<select name="dwpf_settings[dwpf_stylesheet_load_type]" id="dwpf_stylesheet_load_type">
		<?php
		foreach($htmlOptions as $key => $value) {
			$sel = '';
			if($key == $selected) {
				$sel = ' selected="selected"';
			}
			?>
			<option value="<?php echo $key; ?>" <?php echo $sel; ?>><?php echo $value; ?></option>
			<?php
		}
		?>
	</select>
	<p>Uitgesteld laden zorgt ervoor dat de pagina sneller zichtbaar wordt maar kan op trage verbindingen kort flitsen.</p>
	<?php

}
function dwpf_disable_emoji_render(  ) {

	$options = get_option( 'dwpf_settings' );
	$emojiDisabled = false;
	if(isset($options['dwpf_disable_emoji']) && $options['dwpf_disable_emoji'] == '1') {
		$emojiDisabled = true;
	}
	?>
	<input type='checkbox' name='dwpf_settings[dwpf_disable_emoji]' <?php checked( $emojiDisabled, 1 ); ?> value='1'>
	<p>Schakel WordPress Emoji's uit. Dit zorgt voor minder verzoeken naar de server.</p>
	<?php

}


function dwpf_combined_filename_render(  ) {

	$options = get_option( 'dwpf_settings' );
	?>
	<input type="text" name='dwpf_settings[dwpf_combined_filename]' value="<?php echo $options['dwpf_combined_filename']; ?>" />
	<p>Dit is de naam van het gecombineerde CSS bestand.</p>
	<?php

}


function dwpf_cache_lifetime_render(  ) {

	$options = get_option( 'dwpf_settings' );
	?>
	<input type="number" name='dwpf_settings[dwpf_cache_lifetime]' value="<?php echo $options['dwpf_cache_lifetime']; ?>" />
	<p>Geeft aan hoeveel seconden een gecachte stylesheet bewaard mag blijven.</p>
	<?php

}

function dwpf_invalidate_cache_render(  ) {
	?>
	<a href="?page=wperformance&wperformance=invalidate" class="button button-primary">Cache legen</a>
	<?php

}


function dwpf_settings_section_callback(  ) {

	echo __( 'Gebruik deze plugin om de prestaties van uw website te verbeteren.', 'wperformance' );

}


function dwpf_options_page(  ) {

	?>
	<form action='options.php' method='post'>

		<h1>WPerformance</h1>
		<?php
		if(isset($_GET['wperformance']) && $_GET['wperformance'] == 'cache_cleared') {
			?>
			<h3 style="color: forestgreen;">Cache geleegd!</h3>
			<p>Bezoek de front-end om cache bestanden aan te maken.</p>
			<?php
		}
		?>

		<?php
		settings_fields( 'pluginPage' );
		do_settings_sections( 'pluginPage' );
		submit_button();
		?>

	</form>

	<p>Plugin gemaakt door Daniël van der Linden. Zie <a href="https://github.com/Donool/WPerformance" target="_blank">GitHub</a> of <a href="https://spaggel.nl" target="_blank">Spaggel.nl</a>.</p>
	<?php

}
<?php namespace Donool\WPerformance;

use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use MatthiasMullie\PathConverter\Converter;

class Optimizer {

	private $cssMinifier;
	private $jsMinifier;

	private $css;
	private $js;

	public function __construct()
	{
		$this->cssMinifier = new CSS();
		$this->jsMinifier = new JS();
		return $this;
	}

	/**
	 * Add a stylesheet to combine and minify
	 * @param string $source file contents
	 */
	public function addStylesheet($source)
	{
		$this->getCssMinifier()->add($source);
	}

	/**
	 * Return if the cache file is expired
	 * @return bool
	 */
	public function cacheExpired()
	{
		// check if the current time minus the cache timout is less than the file modification time
		return time() - WPERFORMANCE_CACHE_TIMEOUT < filemtime(WPERFORMANCE_STYLES_PATH);
	}

	/**
	 * Check if the stylesheet needs to be regenerated
	 * @return bool
	 */
	public function needsRedownload()
	{
		if(!file_exists(WPERFORMANCE_STYLES_PATH)) {
			return true;
		}

		// check lifetime
		if(!$this->cacheExpired()) {
			return true;
		}

		return false;
	}

	/**
	 * Replace relative urls in css with absolute urls
	 * @param $stylesheet
	 * @param $source
	 *
	 * @return bool|mixed
	 */
	public function fixTemplatePaths( $stylesheet, $source) {
		// Regex to match all urls
		$matchUrls = '/(url|src)(:)*\(\s*[\'"]?\/?(.+?)[\'"]?\s*\)/im';
		preg_match_all($matchUrls, $stylesheet, $urls);

		// only match the url part of the string to bypass a lot of mess
		$assetUrls = $urls[count($urls) - 1];
		// try to parse the url and get the path, we dont need query strings, hosts etc.
		$url = parse_url($source);
		$parts = explode('/', $url['path']);

		// if the first part is empty, we dont need to and just remove it
		if($parts[0] == '') {
			array_shift($parts);
		}
		// remove wp-content from the parts, we already have this
		array_shift($parts);
		// remove the filename from the parts, e.g. substyle.css
		array_pop($parts);

		// try to correct all matched urls in the stylesheet
		foreach ($assetUrls as $assetUrl) {
			// detech how many folders we should go up based on the location of our stylesheet uri
			$goUp = substr_count($assetUrl, '../');
			// remove the amount of parts from the end of the $parts array by slicing $goUp parts from it
			$startPath = array_slice($parts, 0, -$goUp);
			// we have the path to the file now, add the WP_CONTENT_DIR as start, fix some slashes and create our path
			// by imploding the parts we had left with slashes
			$pathToSource = realpath(WP_CONTENT_DIR . '/'. implode('/', $startPath)) . '/';
			// we just have to remove all ../'s from the matched url (using the regex) because we removed all relative stuff
			$correctUrl = $pathToSource . str_replace('../', '', $assetUrl);
			// now we create the url by getting the site url, removing the root to wordpress from our realpath (otherwise you
			// would get something like http://site.com//var/www/wpfolder/assets/subfolder.css)
			$correctUrl = get_site_url() . str_replace(ABSPATH, '/', $correctUrl);

			if($pathToSource === false) {
				continue;
			}

			// update the stylesheet by replacing the matched (relative) url with the fixed absolute urls
			$stylesheet = str_replace($assetUrl, $correctUrl, $stylesheet);
		}
		return $stylesheet;
	}

	/**
	 * @return \MatthiasMullie\Minify\CSS
	 */
	public function getCssMinifier() {
		return $this->cssMinifier;
	}

	/**
	 * @param \MatthiasMullie\Minify\CSS $cssMinifier
	 *
	 * @return Optimizer
	 */
	public function setCssMinifier( $cssMinifier ) {
		$this->cssMinifier = $cssMinifier;

		return $this;
	}

	/**
	 * @return \MatthiasMullie\Minify\JS
	 */
	public function getJsMinifier() {
		return $this->jsMinifier;
	}

	/**
	 * @param \MatthiasMullie\Minify\JS $jsMinifier
	 *
	 * @return Optimizer
	 */
	public function setJsMinifier( $jsMinifier ) {
		$this->jsMinifier = $jsMinifier;

		return $this;
	}

	public function outputCssToFile($path) {
		return $this->getCss($path);
	}

	/**
	 * @return mixed
	 */
	public function getCss($path = null) {
		return $this->getCssMinifier()->minify($path);
	}

	/**
	 * Get the minified and gzipped stylesheet
	 * @return string
	 */
	public function getCompressedCss() {
		return $this->getCssMinifier()->gzip();
	}

	/**
	 * @param mixed $css
	 *
	 * @return Optimizer
	 */
	public function setCss( $css ) {
		$this->css = $css;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getJs() {
		return $this->js;
	}

	/**
	 * @param mixed $js
	 *
	 * @return Optimizer
	 */
	public function setJs( $js ) {
		$this->js = $js;

		return $this;
	}
}
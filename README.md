# WPerformance

WPerformance is a plugin that improves the load time of WordPress websites by combining, minifying and caching CSS and JS files.

## Features

- Stylesheet minifying and combining to reduce asset size and requests
- Deferred loading for stylesheets
- Flush cache button in WP-admin

## Upcoming features

- Script support (JS files)
- A (WP-)cronjob to automatically update files and warm up the cache
- Exclude list in WP-admin to prevent caching special files when needed
